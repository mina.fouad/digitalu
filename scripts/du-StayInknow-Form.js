(function ($) {
    //------------------------ Form ------------------------
  
    function validateEmailFormat(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }
  
    function EmailHasValue(obj) {
      return !(obj && obj.val() == "");
    }
  
    $('.rba-form').on("submit", function (e) {
  
      if (!(EmailHasValue($('.email-field')) &&
        validateEmailFormat($('.email-field').val()))) {
        console.log("not valid");
        return false;
        e.preventdefault();
      } else {
        $(".flip-box").addClass("hover");
        $(".flip-box-back").addClass("front");
      }
    });
  
  })(jQuery, document);