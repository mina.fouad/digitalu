$(document).ready(function () {
    var SessionLoaded = false;
    
        XA.component.search.vent.on("results-loaded", function (results) {
            if (results.searchResultsSignature === "upcoming-sessions-results") {
                SessionLoaded = true;
                if (SessionLoaded) {
                    setTimeout(function(){  
                      if(results.dataCount > 0){
                        $(".sessionfooter").css({
                            "display": 'block'
                        });
                      }
                    }, 300);
                }
            }
        });
    });
