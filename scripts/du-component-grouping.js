// "use strict";

// // $ = jQuery;
// (function ($) {


//     // output structer is 
//     //<ul class="GroupingSearchClass"> 
//     //  <li class = "year xxxx" > 
//     //      YEAR
//     //      <ul>
//     //          <li class = "month xx">
//     //                Month
//     //              <ul>
//     //                  <li class = "item">
//     //                  </li>
//     //              </ul>
//     //          </li>
//     //      </ul>
//     //  </li>
//     //</ul>
//     var SearchResultSignture = "GroupingSearchResult";
//     var SearchResultClass = "GroupingSearchClass";
//     var DateAttrbuite = "StartDate";

//     var YearTemplate = function YearTemplate(year) {
//         return $('<li>' + year + '<ul></ul></li>').attr('id', 'y_' + year);
//     };

//     var MonthTemplate = function MonthTemplate(month, year) {
//         return $('<li >' + months[month - 1] + '<ul></ul></li>').attr('id', 'y_' + year + '_m_' + month);
//     };

//     var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

//     var dict = [];

//     $(document).ready(function ($) {

//         XA.component.search.vent.on("results-loaded", function (results) {
//             if (results.searchResultsSignature === SearchResultSignture) {
//                 // Grouping the <li> by year
//                 $("." + SearchResultClass + " li").each(function () {

//                     var date = new Date(Date.parse($(this).find("[" + DateAttrbuite + "]").text()));
//                     var year = date.getFullYear();

//                     // If the grouping <li> doesn't exist, create it
//                     if ($("[id='y_" + year + "']").length === 0) {
//                         $("." + SearchResultClass + " .search-result-list").append(YearTemplate(year));
//                         dict[dict.length] = {
//                             'year': year,
//                             'months': []
//                         };
//                     }

//                     // Add the current <li> to the corresponding grouping <li>
//                     $(this).appendTo($("[id='y_" + year + "'] ul"));
//                 });

//                 // Grouping the <li> by month inside each grouping <li>
//                 $("[id^= 'y_'] ul").each(function () {

//                     var $this = $(this);
//                     $this.children("li").each(function () {

//                         var date = new Date($(this).find("[" + DateAttrbuite + "]").text());
//                         var month = date.getMonth() + 1;
//                         var year = date.getFullYear();

//                         // If the grouping <li> doesn't exist, create it
//                         if ($this.find("[id='y_" + year + "_m_" + month + "']").length === 0) {
//                             $this.append(MonthTemplate(month, year));
//                             dict.find(function (x) {
//                                 return x.year === year;
//                             }).months.push(month);
//                         }
//                         // Add the current <li> to the corresponding grouping <li>
//                         $(this).appendTo($this.find("[id='y_" + year + "_m_" + month + "'] ul"));
//                     });
//                 });

//                 $('<nav class="Calendar_filter"><ul id = "monthpages" class="Calendar_filter__list" ></ul></nav>').insertBefore("." + SearchResultClass);
//                 $.each(dict, function (k1, val1) {
//                     var year = val1.year;
//                     $.each(val1.months, function (key, value) {
//                         $('#monthpages').append('<li class="Calendar_filter__listItem" ><a>' + months[value - 1] + '</a></li>').click(function () {
//                             $('#y_' + year + '_m_' + value).get(0).scrollIntoView();
//                         });

//                         //  $('#monthpages').append('<li onclick="alert(' + value + ')">' + value + '</li>');
//                     });
//                 });
//             }
//         });
//     });
// })(jQuery)