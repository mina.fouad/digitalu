
$(document).ready(function () {
    if ($('.side-nav').length) {

        
        var topSpacing;
        var isMobileView = false;
        var sideNavElem = $('.side-nav');
        var sideNavHeight;
        var sideNavElemOffset;
        var sectionsElements; 
        

        function initializeSideNav(){

            isMobileView = window.matchMedia('(max-width: 991px)').matches;
            isMobileView ? topSpacing = 200 : topSpacing = 150;
            sideNavHeight = $(sideNavElem).outerHeight();
            sideNavElemOffset = $(sideNavElem).offset();

            //get all section elements and offset 
            sectionsElements = [];
            $(sideNavElem).find('li.item a').each(function () {
                var elemReference = '#' + $(this).attr('target');
                var topOffset = $(elemReference).offset();


                //check if element exists
                if ($(elemReference).length) {
                    var elementRefOffset = {
                        elem: elemReference,
                        top: topOffset.top,
                        bottom: $(elemReference).outerHeight() + topOffset.top,
                    }
                    //console.log(elemReference + " top: " + elementRefOffset.top + " bottom: " + elementRefOffset.bottom + "\n");
                    sectionsElements.push(elementRefOffset);
                }
            });

        }

        $(window).load(function () {

            $('.renameSectionId').each(function(){
                $(this).attr('id',  $(this).attr('id').replace(/ /g,"_").replace('/','-'));  
            })

            initializeSideNav();
            isMobileView ? startMobileSideNav() : startSideNav() ;


        });


        XA.component.search.vent.on("results-loaded", function (results) {
            //if (results.searchResultsSignature === "upcoming-sessions-results") {


                setTimeout(function () {
                    initializeSideNav();
                    isMobileView ? startMobileSideNav() : startSideNav() ;
                }, 1000);

            //}
        }); 

        $(window).resize(function () {

            setTimeout(function () { 
            initializeSideNav();
            isMobileView ? startMobileSideNav() : startSideNav() ;
            $('html,body').animate({
                scrollTop: window.pageYOffset + 1
            }, 0);
        }, 500);

        });




        function startSideNav() {

            //initialize side-nav
            $(sideNavElem).css('top', sectionsElements[0].top);
            $(sideNavElem).css('visibility', 'visible');
            moveSideNav();
 
            $(window).on('scroll', function () {

                moveSideNav(); 
            });

            var currentScrollOffset;
            function moveSideNav() {
                currentScrollOffset = this.pageYOffset;
                if (currentScrollOffset + topSpacing < sectionsElements[0].top) // to active the first item
                {
                    //console.log(1)
                    var currentActiveID = $(sectionsElements[0].elem).attr('id');
                    $('.side-nav a[target*="' + currentActiveID + '"]').addClass('active');
                    $('.side-nav a:not([target*="' + currentActiveID + '"])').removeClass('active');

                    $(sideNavElem).css('position', 'absolute');
                    $(sideNavElem).css('top', sectionsElements[0].top); // move the side-nav
                    
                }

                else if (currentScrollOffset + topSpacing > sectionsElements[0].top && currentScrollOffset + topSpacing < sectionsElements[sectionsElements.length - 1].bottom - sideNavHeight) 
                {
                    //console.log(2)

                    sideNavElemOffset = $(sideNavElem).offset();
                    // add active section
                    for (var x = 0; x < sectionsElements.length; x++) {
                        if (sideNavElemOffset.top + topSpacing >= sectionsElements[x].top) {
                            var currentActiveID = $(sectionsElements[x].elem).attr('id');
                            $('.side-nav a[target*="' + currentActiveID + '"]').addClass('active');
                            $('.side-nav a:not([target*="' + currentActiveID + '"])').removeClass('active'); 

                        }
                    }

                    $(sideNavElem).css('position', 'fixed');
                    $(sideNavElem).css('top', topSpacing);
                }

                else if(currentScrollOffset + topSpacing >= sectionsElements[sectionsElements.length - 1].bottom - sideNavHeight)
                {
                    //console.log(3)
                   $(sideNavElem).css('position', 'absolute');
                   $(sideNavElem).css('top', sectionsElements[sectionsElements.length-1].bottom - sideNavHeight); 
                    
                }
            }

        }

        function startMobileSideNav()
        {
            $('.mobileTitle.active').removeClass('active');
            $('.side-nav .items.active').removeClass('active');
            $('.side-nav.mobileSticky').removeClass('mobileSticky');
            //initialize side-nav
            $(sideNavElem).css('position', 'static');
            $(sideNavElem).css('visibility', 'visible'); 

             

            moveMobileSideNav();
 
            $(window).on('scroll', function () {

                moveMobileSideNav(); 
            });

            var currentScrollOffset;
            function moveMobileSideNav() {
                currentScrollOffset = this.pageYOffset;

                
               //if (sideNavElemOffset.top - 155 <= currentScrollOffset) {
                if (sectionsElements[0].top - 185 <= currentScrollOffset) {
                    $('.side-nav').addClass('mobileSticky');
                }
                else { 
                    $('.side-nav').removeClass('mobileSticky');
                    $(sideNavElem).css('position', 'static');
                }
            }
        } 


        function scrollToAnchor(aid) {
            var aTag = $("#" + aid);
            var elemetOffSet = aTag.offset();
            setTimeout(function () {
            $('html,body').animate({
                scrollTop: elemetOffSet.top - topSpacing
            }, 'slow');
        }, isMobileView ? 500 : 0);
        }

        //click on side-nav item - scroll effect
        $(".side-nav li.item a").click(function () {

            scrollToAnchor($(this).attr('target'));
            $('.mobileTitle').toggleClass('active');
            $(".side-nav .page-list .items").toggleClass('active').slideToggle(); 
        });

        $('.mobileTitle').click(function () {
            $(this).toggleClass('active');
            $(".side-nav .page-list .items").toggleClass('active').slideToggle();

        })


    }
});