/* global _:true*/
(function ($) {
    const onPreventDefault = (event) => {
        event.preventDefault();
      };
      const handleDisableScroll = () => {
        window.document.body.addEventListener('touchmove', onPreventDefault, {
          passive: false
        });
      };
    
      const handleEnableScroll = () => {
        window.document.body.removeEventListener('touchmove', onPreventDefault, {
          passive: false
        });
      };

    function stopBodyScrolling(bool) {
        if (bool === true) {
            handleDisableScroll();

         
        } else {
            handleEnableScroll();
        }
    }


    $('body').on('click , touchstart', '.popup__overlay', function (event) {
        var outside = (event.target.classList).contains('popup__overlay');
        $('body').on('click', '.highlight', showpopup);

        if (outside) {
            $(".popup").css({
                "opacity": 0
            });
            $('body').removeClass('locked');
            $('body').removeAttr('touch-action');
            stopBodyScrolling(false);
            window.requestAnimationFrame(function () {
                $(".popup__overlay").hide();
            });
            $(".popup__highlight").removeClass(function (index, className) {
                return (className.match(/(^|\s)n\S+/g) || []).join(' ');
            });
        }

    });

    function showpopup() {
        var data = $(this).find(".highlight__card").html();
        $(".popup__data").html(data);

        $(".popup__highlight").html($(this).html()).addClass("n" + ($(this).index() + 1));

        $(".popup__overlay").css({
            "display": "flex"
        });
        window.requestAnimationFrame(function () {
            $(".popup").css({
                "opacity": 1
            });

            $('body').addClass('locked');
            $('body').attr('touch-action','none');

            stopBodyScrolling(true);

        });
        $('body').off('click', '.highlight', showpopup);
    }

    $('body').on('click', '.highlight', showpopup);

    $('body').on('click touchstart', '.popup__close-btn', function (event) {
        $(".popup").css({
            "opacity": 0
        });

        $('body').removeClass('locked');
        $('body').removeAttr('touch-action');

        stopBodyScrolling(false);

        window.requestAnimationFrame(function () {
            $(".popup__overlay").hide();
        });
        $(".popup__highlight").removeClass(function (index, className) {
            return (className.match(/(^|\s)n\S+/g) || []).join(' ');
        });
        event.stopPropagation();
        setTimeout(function () {
            $('body').on('click', '.highlight', showpopup);
        }, 300);
    });

    $(window).on('DOMContentLoaded load resize', function () {
        if ($(window).width() < 768) {
            $(".highlight__container > .component-content").not('.slick-initialized').slick({
                infinite: false,
                slidesToShow: 2,
                slidesToScroll: 1,
                variableWidth: true,
                mobileFirst: true,
                swipeToSlide: true,
                arrows: false,
                responsive: [{
                    breakpoint: 767,
                    settings: 'unslick'
                }]
            });
            $(".highlight").css("display", "");
            $(".highlight").css("display", "flex");
        }
    });
})(jQuery);

