(function ($) {
    var isMobileView = window.matchMedia('(max-width: 1023px)').matches;
    var topSpacing = 150;

    function scrollToAnchor(aid) {
        var aTag = $("#" + aid);
        var elemetOffSet = aTag.offset();
        setTimeout(function () {
        $('html,body').animate({
            scrollTop: elemetOffSet.top - topSpacing
        }, 'slow');
    }, isMobileView ? 500 : 0);
    }
    $(document).on( 'click', ".next-Session .upcoming-sessions-lnk", function(){
        scrollToAnchor($(this).attr('target'));

    } );

  
  })(jQuery, document);

  