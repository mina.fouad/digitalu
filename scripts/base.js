XA.component.base = (function($) {
    var pub = {};
    
    var _offset = {
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    }

    pub.init = function() {
        /**
         * Returns true if the given element is in viewport.
         */
        $.inViewPort =function (el, offset) {
            if (offset === undefined) {
                offset = _offset;
            }
            // Load element dimensions.
            var rect = el.getBoundingClientRect();

            // Load window dimensions.
            var win = {
                width: (window.innerWidth || document.documentElement.clientWidth),
                height: (window.innerHeight || document.documentElement.clientHeight)
            };

            // Test if vertically in view.
            var vertInView = ((rect.top + offset.top) <= win.height) && ((rect.top + rect.height + offset.bottom) >= 0);

            // Test if horizontally in view.
            var horInView = ((rect.left + offset.left) <= win.width) && (((rect.left + offset.left) + rect.width) >= 0);

            // Return true/false whether the element is overlapping.
            return (vertInView && horInView);
        }
        /**
         * Returns true if the device hardware is a tablet or a mobile view
         */
        $.isMobileOrTabletView = function() {
            return $(window).width() < 1024;
        }
    }

    return pub;

})(jQuery);

XA.register("base", XA.component.base);