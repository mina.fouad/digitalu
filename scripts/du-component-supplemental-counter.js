(function ($) {
    var counterlabel = $('.js__small_card__wrapper__counter');
    var cards = $('.small_card');
    var totalShown;
    var remaining = 0;
    $(window).on('DOMContentLoaded load resize', function () {
        var cardcount = cards.length;
        if ($(window).width() <= 768) {
            //Mobile view
            totalShown = 3;
            if (cardcount > totalShown) {
                counterlabel.show();
            } else {
                counterlabel.hide();
            }

        } else {
            //Desktop view
            totalShown = 6;
            if (cardcount > totalShown) {
                counterlabel.show();
            } else {
                counterlabel.hide();
            }
        }
        remaining = cardcount - totalShown;
        counterlabel.find('span').text(remaining);
        for (var i = 0; i < cardcount; i++) {
            if (cards.eq(i).index() >= totalShown && cards.eq(i).index() < cardcount) {
                cards.eq(i).addClass('small_card__hidden');
            } else {
                cards.eq(i).removeClass('small_card__hidden');
            }
        }
    });
    counterlabel.on('click', function () {
        $.each(cards, function () {
            $(this).removeClass('small_card__hidden');
        });
        counterlabel.hide();
    })
})(jQuery);