(function ($) {

    var totalSlides = 0;
    $(window).on('DOMContentLoaded load resize', function () {
        var currentSlide = $('.slick-track .slick-slide.slick-current').index() + 1;
        totalSlides = $('.slick-slide').length;
        if ($(window).width() < 768) {
            $(".js-interested-in-container > .component-content").not('.slick-initialized').slick({
                infinite: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                mobileFirst: true,
                centerMode: true,
                arrows: true,
                swipeToSlide: true,
                responsive: [{
                    breakpoint: 767,
                    settings: 'unslick'
                }]
            });
            $(".js-interested-in-container .component-content").css("display", "flex");

            if (totalSlides >= 1) {
                $('.slider-indicator .totalSlides').text(totalSlides);
                $('.slider-indicator .currentSlide').text(currentSlide);
            } else {
                $('.slider-indicator .slick-arrow').hide()
            }

        }

        function incrementSlideIndicator(slide) {
            slide = $('.slick-track .slick-slide.slick-current').index();
            $('.slider-indicator .currentSlide').text(slide + 1);
        }
        $('.js-interested-in-container .slick-arrow').on('click', function () {
            incrementSlideIndicator(currentSlide);
        })
        $('.js-interested-in-container .slick-slider').on('swipe', function (event, slick, direction) {
            incrementSlideIndicator(currentSlide);
        });
    });

})(jQuery);