// XA.component.banner = (function ($) {

//     var pub = {};

//     function ScaledBanner(element) {
//         var _scale = 0;
//         var _scaleSpeed = 0.0005;

//         function preform() {
//             _scale = _scale < 1 ? 1 : 1 + (window.scrollY * _scaleSpeed);
//             var transform = "scale(" + _scale + ")";

//             window.requestAnimationFrame(function () {
//                 element.css({
//                     "transform": transform
//                 });
//             });
//         }
//         ScaledBanner.prototype.monitorScroll = function () {
//             $(document).scroll(preform);
//         }
//     }



//     pub.init = function () {
//         var heroImages = $(".hero__image img");
//         var banner = new ScaledBanner(heroImages);
//         banner.monitorScroll();
//     };

//     return pub;

// })(jQuery);
// XA.register('banner', XA.component.banner);