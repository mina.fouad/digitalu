$(document).ready(function () {
    var seeMore = $('.seeMore');
    var Courses = $('.courseName');
    var totalcourcses=6;
    var remainingCources = 0;
    var coursesCount;
    $(window).on('load resize', function () {
        seeMore.hide();
        coursesCount = Courses.length;
        totalcourcses = 6;
            if (coursesCount > totalcourcses) {
                seeMore.show();
            } else {
                seeMore.hide();
            }
        
        remainingCources = coursesCount - totalcourcses;

        seeMore.find('span').text(remainingCources);
        for (var i = 0; i < coursesCount; i++) {
            if(i > 5){
                $(Courses[i]).addClass('hideCourse');
            }
        }
    });
    seeMore.on('click', function () {
        for( var q=0; q < coursesCount; q++){
            $(Courses[q]).removeClass('hideCourse');
        }
        seeMore.hide();
    })
});
