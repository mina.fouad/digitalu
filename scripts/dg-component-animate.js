/* global _:true*/
XA.component.animate = (function ($) {

    var pub = {};

    var els, elsCount;


    function _toggleVisibility(el) {
        if (el.classList.contains('off-screen')) {
            window.requestAnimationFrame(function() {
                el.classList.remove('off-screen');
                elsCount--;
            });
        }
    }

    function _check() {
        els.each(function () {
            if ($.inViewPort(this)) {
                _animate(this);
            }
        });
    }

    function _animate(el) {
        _toggleVisibility(el);
        if (elsCount <= 0) {
            $(document).off("scroll", _onScroll);
        }
    }

    function _onScroll() {
        _check();
    }

    pub.init = function () {
        els = $(".off-screen");
        elsCount = els.length;
        _check();

        if (elsCount > 0) {
            $(document).on("scroll", _onScroll);
        } else {
            $(document).off("scroll", _onScroll);
        }
    }

    return pub;

})(jQuery);

XA.register('animate', XA.component.animate);