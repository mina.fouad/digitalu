// remove ribbon in preview mode
//$('body.preview #scCrossPiece, body.preview #scPageExtendersForm').hide(); 

// IE hack
var isIE = '-ms-scroll-limit' in document.documentElement.style && '-ms-ime-align' in document.documentElement.style;
if(isIE){
$('body').addClass('ie');
}

// disable zoom in safari 10
document.addEventListener('touchmove', function(event) {
  event = event.originalEvent || event;
  if (event.scale !== 1) {
     event.preventDefault();
  }
}, false);

 

(function ($) {
  var headerHeight = $(".nav-top").innerHeight(); 
  var bottomNav = $(".nav-bottom");
  // var topNav = $(".nav-top"); 
  var bannerHeight = bottomNav.innerHeight();
  var hero = $(".hero__wrap");
  var menu = $(".nav-bottom.container-fluid");
  var closeBtn = $(".navbar__toggle");
  var headerLinks = $(".du__home .header-links ul li");
  var SCALE_SPEED = 0.0005;
  var scale = 1;
  var editMode;
  ////////////////////////////////////////////
  $(window).on("scroll", function () {
    heroZoomOnScroll($(".hero__image img"));
    if (($(window).scrollTop() > headerHeight) & ($(window).width() >= 991)) {
      bottomNav.addClass("nav-fixed");
      // topNav.addClass("d-none");
      hero.addClass("nav-fixed--hero");
    } else {
      bottomNav.removeClass("nav-fixed");
      // topNav.removeClass("d-none");
      hero.removeClass("nav-fixed--hero");
    }
  });
  closeBtn.on("click", function () {
    $(this).toggleClass("close-btn");
    menu.toggleClass("menu-open");
  });
  headerLinks.not('.item4 , .item3').find('a').on('click', function () {
    // e.preventDefault();
      var section = $(this).attr("href");
      var position = $(section).offset().top;
      if ($('body').hasClass('on-page-editor') || $('body').hasClass('preview')) {
        editMode = 90;
      } else {
        editMode = 0;
      }
      if (menu.hasClass('menu-open')) {
        menu.removeClass('menu-open')
        closeBtn.removeClass("close-btn");
      }
      $('body , html').animate({
        scrollTop: position - bannerHeight - editMode
      }, 700)
  });

  function heroZoomOnScroll($elem) {
    var scroll = $(window).scrollTop();
    if ($elem.length > 0 && scroll < $elem.innerHeight()) {
      scale = scale < 1 ? 1 : 1 + (scroll * SCALE_SPEED);
      $elem.css({
        transform: "scale(" + scale + ")"
      });
    }
  }

})(jQuery, document);